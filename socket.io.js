module.exports = function(server) {
    var io = require('socket.io')(server);
    return {
        config: function() {
            io.on('connection', function(socket){
                
                socket.on('echo', function (data) {
                    io.sockets.emit('message', data);
                });

                socket.on('message', function (data) {
                    console.log(data);
                    socket.broadcast.to(data.room).emit('message', message);
                });

                socket.on('offer', function (data) {
                    io.sockets.emit('message', data);
                });

                socket.on('create or join', function (room) {
                    var numClients = io.sockets.clients(room).length;

                    console.log('Room ' + room + ' has ' + numClients + ' client(s)');
                    console.log('Request to create or join room', room);

                    if(numClients == 0) {
                        socket.join(room);
                        socket.emit('created', room);
                    } 

                    else if(numClients > 0) {
                        io.sockets.in(room).emit('join', room);
                        socket.join(room);
                        socket.emit('joined', room);
                    } 
                    
                    socket.emit('emit(): client ' + socket.id + ' joined room ' + room);
                    socket.broadcast.to(room).emit('broadcast(): client ' + socket.id + ' joined room ' + room);
                });

                socket.on('leave', function (room) {
                    socket.leave(room);
                    socket.broadcast.to(room).emit("message");
                });

                socket.on('disconnect', function(){});
            });
        }
    };
};